# sokoban

A GEM implementation of the Sokoban game for Atari computers. 

Compiling
---------

The program can be compiled on an Atari using the 
[AHCC C Compiler](http://members.chello.nl/h.robbers/).  

* sokoban.prj is the project file for a Firebee/Coldfire build. 
* soko_fal.prj is the project file for a Falcon build.
* soko_st.prj is the project file for an ST/68000 build.

(On version 5.3 of AHCC, you need to include the definitions of WM_SHADED 
and WM_UNSHADED from SINCLUDE\AES.H in INCLUDE\AES.H.)

