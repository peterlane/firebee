# BibFind

This program lets you search for entries in a bibliography 
written in BibTeX .bib format, and either copy them to a word processor 
or text editor using the system clipboard, or mark them to save as 
a complete reference list.

Compiling
---------

The program can be compiled on an Atari using the AHCC C Compiler.  

* bibfind.prj is the project file to build a standalone program.  Read 
the comments in bibfind.prj about the RSC file. 
* bibacc.prj is the project file to build as an accessory.  Read the 
comments in bibacc.prj about the RSC file.
* bibtest.prj builds a test program to ensure the biblio parsing is correct.

(On version 5.3 of AHCC, you need to copy the definitions of FL3D... 
from SINCLUDE\AES.H to INCLUDE\AES.H when compiling the .ACC.)

