#if !defined(WINDOWS_H)
#define WINDOWS_H

/* include some standard GEM and C headers */
#include <aes.h>
#include <gemf.h>
#include <vdi.h>

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "version8.rsh"
#include "eg_draw.h"
#include "readfile.h"

#define MIN_WIDTH 60
#define MIN_HEIGHT 100

/* structure to hold data relevant to our window */
struct win_data {
	int handle; 	/* identifying handle of the window */

	char ** text; /* text to display in window */
	bool free_text; /* flag to indicate if text is from file, and should be freed */

	int cell_h;
	int cell_w;
	int vert_posn;
	int horz_posn;
	int lines_shown;
	int colns_shown;

	struct win_data * next; /* pointer to next window in list */
};

void start_program (void);
void open_vwork (void);

#endif
