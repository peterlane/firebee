#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "readfile.h"

#define MAXLINE 1000 /* maximum permitted line length */

/* From K&R book: read a line of text into s, returning length */
int getline (FILE * fp, char s[], int lim, bool keep_newline) {
	int c, i;

	i = 0;
	while (--lim > 0 && (c = fgetc(fp)) != EOF && c != '\n') {
		s[i++] = c;
	}
	if (keep_newline && c == '\n') {
		s[i++] = c;
	}
	s[i] = '\0';

	return i;
}

/* count the number of lines in the file */
int count_lines (char * filename) {
	int size = 0;
	FILE * fp = fopen (filename, "r");

	if (fp != NULL) {
		char s[MAXLINE];

		do {
			getline (fp, s, MAXLINE, false);
			size += 1;
		} while (!feof (fp));

		fclose (fp);
	}

	return size;
}

/* Read text from file, store as an array of strings,
   and return a pointer to it.  Caller is responsible
   for freeing the space used.
   Returns NULL if file could not be read.
 */
char ** read_from_file (char * filename) {
	char ** text;
	int nlines = count_lines (filename);
	int i;
	char s[MAXLINE]; /* space to store read line */
	FILE * fp = fopen (filename, "r");

	if (fp == NULL) return NULL; /* failed to open file */

	text = malloc (sizeof(char *) * (nlines+1));

	for (i = 0; i < nlines && !feof(fp); i += 1) {
		getline (fp, s, MAXLINE, false);
		text[i] = strdup (s);
	}
	text[nlines] = 0; /* end marker */

	return text;
}