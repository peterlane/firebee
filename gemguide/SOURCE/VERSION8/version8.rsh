 /*  Resource C-Header-file v1.95 for ResourceMaster v2.06&up by ARDISOFT  */

#define MAIN_MENU 0  /* menu */
#define MAIN_MENU_ABOUT 8  /* STRING in tree MAIN_MENU */
#define MAIN_MENU_OPEN 17  /* STRING in tree MAIN_MENU */
#define MAIN_MENU_QUIT 19  /* STRING in tree MAIN_MENU */
#define MAIN_MENU_BLAKE 21  /* STRING in tree MAIN_MENU */
#define MAIN_MENU_KEATS 22  /* STRING in tree MAIN_MENU */
#define MAIN_MENU_WORDSWORTH 23  /* STRING in tree MAIN_MENU */
