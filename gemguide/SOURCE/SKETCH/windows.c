#include "windows.h"

/* GEM arrays */
int work_in[11],
	work_out[57],
	contrl[12],
	intin[128],
	ptsin[128],
	intout[128],
	ptsout[128];

int app_handle; /* application graphics handle */

void event_loop (struct win_data * wd);

void do_fulled (struct win_data * wd);
void do_redraw (struct win_data * wd, GRECT * rec1);
void do_sized (struct win_data * wd, int * msg_buf);

void set_clip (bool flag, GRECT rec);
void draw_interior (struct win_data * wd, GRECT clip);

bool is_full_window (struct win_data * wd);

/* open window and enter event loop */
void start_program (void) {
	struct win_data wd;
	int fullx, fully, fullw, fullh;

	graf_mouse (ARROW, 0L); /* ensure mouse is an arrow */

	/* 1. set up and open our window */
	wind_get (0, WF_WORKXYWH, &fullx, &fully, &fullw, &fullh);
	wd.handle = wind_create (NAME|CLOSER|FULLER|MOVER|SIZER|INFO, fullx, fully, fullw, fullh);
	wind_set (wd.handle, WF_NAME, "Sketch", 0, 0);
	wind_set (wd.handle, WF_INFO, "Click, move and release mouse to draw", 0, 0);
	wind_open (wd.handle, fullx, fully, 300, 200);

	/* 2. process events for our window */
	event_loop (&wd);

	/* 3. close and remove our window */
	wind_close (wd.handle);
	wind_delete (wd.handle);
}

/* Standard code to set up GEM arrays and open work area.
 * In particular, this gets app_handle, which is needed to use the screen.
*/
void open_vwork (void) {
	int i;
	int dum;

	app_handle = graf_handle (&dum, &dum, &dum, &dum);
	work_in[0] = 2 + Getrez ();
	for (i = 1; i < 10; work_in[i++] = 1);
	work_in[10] = 2;
	v_opnvwk (work_in, &app_handle, work_out);
}

void event_loop (struct win_data * wd) {
	EVENT ev;
	bool in_line = false;
	int pxy[4];

	ev.ev_mflags = MU_BUTTON | MU_M1 | MU_M2 | MU_MESAG;

	do {
		int event_type;
		int x, y, w, h;

		/* if we have not started a line, look for left button down
		   else look for left button up
		 */
		ev.ev_mbclicks = 1; /* look for a single click */
		ev.ev_bmask = 1; /* look for the left button */
		ev.ev_mbstate = (in_line ? 0 : 1); /* 0 if in line, is button up */

		/* set the dimensions of the window in M1 and M2 events
		   this must be done every loop, as window size may change
		 */
		wind_get (wd->handle, WF_WORKXYWH, &x, &y, &w, &h);
		ev.ev_mm1flags = false;
		ev.ev_mm1x = x;
		ev.ev_mm1y = y;
		ev.ev_mm1width = w;
		ev.ev_mm1height = h;
		ev.ev_mm2flags = true;
		ev.ev_mm2x = x;
		ev.ev_mm2y = y;
		ev.ev_mm2width = w;
		ev.ev_mm2height = h;

		event_type = EvntMulti (&ev);

		/* If left button clicked / released; start / draw line */
		if (event_type & MU_BUTTON) {
			if (in_line) {
				/* finished line, so draw line and start again */
				pxy[2] = ev.ev_mmox;
				pxy[3] = ev.ev_mmoy;
                                graf_mouse (M_OFF, 0L);
				v_pline (app_handle, 2, pxy);
                                graf_mouse (M_ON, 0L);

				in_line = false;
			} else {
				/* starting line, so record position */
				pxy[0] = ev.ev_mmox;
				pxy[1] = ev.ev_mmoy;
				in_line = true;
			}
		}

		/* If mouse entered our window, cursor to cross hair */
		if (event_type & MU_M1) {
			graf_mouse (THIN_CROSS, 0L);
		}

		/* If mouse left our window, cursor to arrow */
		if (event_type & MU_M2) {
			graf_mouse (ARROW, 0L);
		}

		if (event_type & MU_MESAG) {
			switch (ev.ev_mmgpbuf[0]) {

				case WM_TOPPED:
					wind_set (ev.ev_mmgpbuf[3], WF_TOP, 0, 0);
					break;

				case WM_MOVED:
					wind_set (ev.ev_mmgpbuf[3], WF_CURRXYWH, ev.ev_mmgpbuf[4],
						ev.ev_mmgpbuf[5], ev.ev_mmgpbuf[6], ev.ev_mmgpbuf[7]);
					break;

				case WM_FULLED:
					do_fulled (wd);
					break;

				case WM_SIZED:
					do_sized (wd, ev.ev_mmgpbuf);
					break;

				case WM_REDRAW:
					do_redraw (wd, (GRECT *)&ev.ev_mmgpbuf[4]);
					break;
			}
		}
	} while (ev.ev_mmgpbuf[0] != WM_CLOSED);

}

/* called when the full-box is clicked.
   Window will be made full or return to its previous size, depending
   on current state.
 */
void do_fulled (struct win_data * wd) {
	if (is_full_window (wd)) { /* it's full, so shrink to previous size */
		int oldx, oldy, oldw, oldh;
		int fullx, fully, fullw, fullh;

		wind_get (wd->handle, WF_PREVXYWH, &oldx, &oldy, &oldw, &oldh);
		wind_get (wd->handle, WF_FULLXYWH, &fullx, &fully, &fullw, &fullh);
		graf_shrinkbox (oldx, oldy, oldw, oldh, fullx, fully, fullw, fullh);
		wind_set (wd->handle, WF_CURRXYWH, oldx, oldy, oldw, oldh);

	} else { /* make full size */
		int curx, cury, curw, curh;
		int fullx, fully, fullw, fullh;

		wind_get (wd->handle, WF_CURRXYWH, &curx, &cury, &curw, &curh);
		wind_get (wd->handle, WF_FULLXYWH, &fullx, &fully, &fullw, &fullh); // <2>
		graf_growbox (curx, cury, curw, curh, fullx, fully, fullw, fullh);
		wind_set (wd->handle, WF_CURRXYWH, fullx, fully, fullw, fullh);
	}
}

/* Called when application asked to redraw parts of its display.
   Walks the rectangle list, redrawing the relevant part of the window.
 */
void do_redraw (struct win_data * wd, GRECT * rec1) {
	GRECT rec2;

	wind_update (BEG_UPDATE);

	wind_get (wd->handle, WF_FIRSTXYWH, &rec2.g_x, &rec2.g_y, &rec2.g_w, &rec2.g_h);
	while (rec2.g_w && rec2.g_h) {
		if (rc_intersect (rec1, &rec2)) {
			draw_interior (wd, rec2);
		}
		wind_get (wd->handle, WF_NEXTXYWH, &rec2.g_x, &rec2.g_y, &rec2.g_w, &rec2.g_h);
	}

	wind_update (END_UPDATE);
}

/* called when the window is resized.  It resizes the current window
   to the new dimensions, but stops the new dimensions going beyond
   the minimum allowed height and width.
   */
void do_sized (struct win_data * wd, int * msg_buf) {
	if (msg_buf[6] < MIN_WIDTH) msg_buf[6] = MIN_WIDTH;
	if (msg_buf[7] < MIN_HEIGHT) msg_buf[7] = MIN_HEIGHT;

	wind_set (wd->handle, WF_CURRXYWH, msg_buf[4], msg_buf[5], msg_buf[6], msg_buf[7]);
}

/* sets/unsets clipping rectangle in VDI */
void set_clip (bool flag, GRECT rec) {
	int pxy[4];

	pxy[0] = rec.g_x;
	pxy[1] = rec.g_y;
	pxy[2] = rec.g_x + rec.g_w - 1;
	pxy[3] = rec.g_y + rec.g_h - 1;

	vs_clip (app_handle, flag, pxy);
}

/* Draw interior of window, within given clipping rectangle */
void draw_interior (struct win_data * wd, GRECT clip) {
	int pxy[4];
	int wrkx, wrky, wrkw, wrkh; /* some variables describing current working area */

	/* set up drawing, by hiding mouse and setting clipping on */
	graf_mouse (M_OFF, 0L);
	set_clip (true, clip);
	wind_get (wd->handle, WF_WORKXYWH, &wrkx, &wrky, &wrkw, &wrkh);

	/* clears the display */
	vsf_color (app_handle, WHITE);
	pxy[0] = wrkx;
	pxy[1] = wrky;
	pxy[2] = wrkx + wrkw - 1;
	pxy[3] = wrky + wrkh - 1;
	vr_recfl (app_handle, pxy);

	/* tidies up */
	set_clip (false, clip);
	graf_mouse (M_ON, 0L);
}

/* calculates if window is currently at maximum size */
bool is_full_window (struct win_data * wd) {
	int curx, cury, curw, curh;
	int fullx, fully, fullw, fullh;

	wind_get (wd->handle, WF_CURRXYWH, &curx, &cury, &curw, &curh);
	wind_get (wd->handle, WF_FULLXYWH, &fullx, &fully, &fullw, &fullh);
	if (curx != fullx || cury != fully || curw != fullw || curh != fullh) {
		return false;
	} else {
		return true;
	}
}
