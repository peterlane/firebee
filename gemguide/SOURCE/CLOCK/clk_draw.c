#include "clk_draw.h"

#define R 3

void draw_binary (int app_handle, int x, int y, int val, int nbits) {
	int bit = 1;
	int i;

	for (i = 0; i < nbits; i += 1) {
		vsf_color (app_handle, (val & bit ? GREEN : WHITE));
		v_circle (app_handle, x-(i*(2*R+1)), y, R);
		bit *= 2;
	}
}

void draw_clock (int app_handle, struct win_data * wd, int x, int y, int w, int h) {
	time_t lt;
	struct tm * ptr;

	lt = time (NULL);
	ptr = localtime (&lt);

	draw_binary (app_handle, x+(5*(2*R+1)), y+10, ptr->tm_hour, 5);
	draw_binary (app_handle, x+(12*(2*R+1)), y+10, ptr->tm_min, 6);
	draw_binary (app_handle, x+(19*(2*R+1)), y+10, ptr->tm_sec, 6);

}
