#if !defined(CLK_DRAW)
#define CLK_DRAW

#include "time.h"
#include "windows.h"

void draw_clock (int app_handle, struct win_data * wd, int x, int y, int w, int h);

#endif