#if !defined(WINDOWS_H)
#define WINDOWS_H

/* include some standard GEM and C headers */
#include <aes.h>
#include <gemf.h>
#include <vdi.h>

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "eg_draw.h"

#define MIN_WIDTH 60
#define MIN_HEIGHT 100

/* structure to hold data relevant to our window */
struct win_data {
	int handle; 	/* identifying handle of the window */

	char ** poem; /* poem to display in window */
};

void start_program (void);
void open_vwork (void);

#endif
