#if !defined(DIALOG_H)
#define DIALOG_H

#include <aes.h>
#include <gemf.h>
#include <vdi.h>
#include <stdio.h>
#include <stdlib.h>

#include "TEMPCONV.rsh"

void start_program (void);
char * get_tedinfo_str (OBJECT * tree, int object);
void open_vwork (void);

#endif
