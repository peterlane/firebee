#include "tempdial.h"

/* GEM arrays */
int work_in[11],
	work_out[57],
	contrl[12],
	intin[128],
	ptsin[128],
	intout[128],
	ptsout[128];

int app_handle; /* application graphics handle */

void start_program (void) {
	int dial_x, dial_y, dial_w, dial_h, choice;
	OBJECT * dial_addr;

	graf_mouse (ARROW, 0);
	if (!rsrc_load("TEMPCONV.rsc")) {
		form_alert(1, "[1][TEMPCONV.rsc missing!][OK]");
	} else {
		char * temp;
		char * result;

		rsrc_gaddr (R_TREE, TEMP, &dial_addr);

		temp = get_tedinfo_str (dial_addr, TEMP_TEMP);
		result = get_tedinfo_str (dial_addr, TEMP_RESULT);

		sprintf (temp, "");

		form_center(dial_addr, &dial_x, &dial_y, &dial_w, &dial_h);
		form_dial(FMD_START, 0, 0, 10, 10, dial_x, dial_y, dial_w, dial_h);

		do {
			objc_draw(dial_addr, 0, 8, dial_x, dial_y, dial_w, dial_h);
			choice = form_do (dial_addr, TEMP_TEMP);

			/* do action */
			switch (choice) {
				case TEMP_CONVERT:
					sprintf (result, "Fahr: %d", 32+(9*atoi(temp))/5);
					break;

			}

			/* revert the button to normal state */
			dial_addr[choice].ob_state = NORMAL;

		} while (choice != TEMP_CLOSE);

		form_dial(FMD_FINISH, 0, 0, 10, 10, dial_x, dial_y, dial_w, dial_h);
	}
}

/* returns a pointer to an editable string in a dialog box */
char * get_tedinfo_str (OBJECT * tree, int object) {
	return tree[object].ob_spec.tedinfo->te_ptext;
}

void open_vwork (void) {
	int i, dum;

	app_handle = graf_handle (&dum, &dum, &dum, &dum);
	work_in[0] = 2 + Getrez ();
	for (i = 1; i < 10; work_in[i++] = 1);
	work_in[10] = 2;
	v_opnvwk (work_in, &app_handle, work_out);
}
