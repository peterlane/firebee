# gemguide

This document is a guide to GEM programming in C using AHCC.

A sample program is constructed step by step, considering each aspect 
of AES window handling in turn.  Later versions feature multiple windows 
with sliders, menus, and some built-in dialogs.  Other example programs 
include a binary clock to illustrate timer events, a simple sketch 
program to illustrate mouse events, and a temperature conversion 
program to illustrate user-defined dialogs.

All examples should compile and run on all Atari computers, 
from the Atari ST to clones such as the Firebee.  

Marcel Schoen has created a German translation of this guide:
https://github.com/marcelschoen/gemguide



