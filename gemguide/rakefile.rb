# The guide is written in Asciidoc. To build the guide you will need Ruby 
# installed and the gems `asciidoctor`, `asciidoctor-pdf` and `rouge`.
# html2pages is also needed to create a paged html version: 
# https://launchpad.net/html2pages

desc 'create html version of guide, for viewing under the fossil repository'
task :html do
  sh 'asciidoctor -n -a source-highlighter=rouge guide.txt'
  sh 'html2pages --title "A Guide to GEM Programming in C using AHCC" -p "Section" -d html guide.html'
  rm 'guide.html'
  cp 'style.css', 'html/page-style.css'
  # add fossil header
  Dir.chdir('html')
  Dir.foreach('.') do |filename|
    next unless filename.end_with?('.html')
    lines = File.new(filename).readlines
    # save with added information
    File.open(filename, 'w') do |file|
      file.puts("<div class='fossil-doc' data-title='Gem Guide'>")
      lines.each do |line| 
        next if line.start_with?('<html ')
        line.gsub!('Home page','Contents')
        line.gsub!('</html>','')
        file.puts(line) 
      end
      file.puts("</div>")
    end
  end
end

desc 'create pdf version of guide'
task :pdf do
  sh 'asciidoctor-pdf -a numbered -a source-highlighter=rouge guide.txt'
end

