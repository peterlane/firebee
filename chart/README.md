# CHART

This project provides a library of simple chart drawing functions for 
Atari C programmers.  The code is written to be embedded into an 
existing project.  The code was written for 
[AHCC](http://members.chello.nl/h.robbers/), 
but may work with other C compilers with minor modifications.

## Use

Include the files chart.h and chart.c in your own project.

Read chart.h for a description of the available functions.

The file example.c illustrates all functions in the library.

When compiling on a Firebee, include the compiler flag PMARKS, to use 
the library's built in pmarker code. 

## Functionality

Three types of chart are supported:

1. Bar Charts: A set of positive values is converted into a series of vertical
   bars.  The title, x/y labels, colours and fill patterns of bars may be
   altered.

2. Line Charts: A set of lines added to a line chart.  The title, x/y labels,
   and colour, line style and point style may all be altered.

3. Pie Charts: A set of positive values is converted into slices of a pie
   chart.  The title, slice labels, colours and fill patterns may be altered.

