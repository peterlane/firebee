# BibFind

BibFind lets you search for entries in a bibliography written in BibTeX .bib
format, and copy them to a word processor or text editor using the system
clipboard. BibFind is written for the Atari platform, but currently is only
compiled and tested on the Firebee, using MINT+XaAES. 

<img src="images/bibfind.jpg" alt="Image of Bibfind program running on Firebee" width="400" />

Above is a screenshot of BibFind running under MINT+XaAES on a Firebee. Two
citations and references have been copied into an Atari Works document. A bar
chart shows a count of the different document types within the BibTeX file.

Features:

*    Displays and allows navigation between records within a .bib file
*    Search tool, to display records matching one or more search terms
*    Ability to copy the reference to the system clipboard, to paste into a word processor. There are four types of copied information:
  *        id: useful for LaTeX or asciidoc documents
  *        cite: an (author, year) citation style
  *        Harvard: a Harvard-style author (year) reference, to paste into reference lists
  *        IEEE: an IEEE-style reference, for numeric reference lists
*    Records may be marked, and later a complete reference list saved to file in Harvard or IEEE format
*    Some simple statistics on the type and year of publications can be displayed in a bar chart
*    Can be run as a PRG or as an ACC

## Download

* Version 1.1.1 (for Firebee MINT+XaAES): [bibfind.zip](/uv/bibfind.zip) - md5sum cdc01282fca316b23a5bd48c990ed4f4
