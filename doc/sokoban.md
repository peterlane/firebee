# Sokoban

A GEM version of the classic puzzle game, Sokoban.  This program should 
work on all Atari machines, from Atari ST to Firebee. 

<img src="images/sokoban.jpg" alt="Screenshot of Sokoban running on a Firebee" width="40%" />

Above is a screenshot of Sokoban running under MINT+XaAES on a Firebee.

<img src="images/soko_st.png" alt="Screenshot of Sokoban running in high resolution on an Atari ST" width="40%" />

And here running in high resolution on an Atari ST.

Features:

* Contains the 50 classic levels from Thinking Rabbit
* Additional levels can be loaded from a text file
* Unlimited undo
* Total moves and pushes per level recorded and saved
* Size of display can be adapted

## Download

* Version 1.1.1: [sokoban.zip](/uv/sokoban.zip) - md5sum 0bc4613e20650eadd8028dd563e9fe0d

