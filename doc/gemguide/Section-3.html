<div class='fossil-doc' data-title='Gem Guide'>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
    "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<head>
<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="generator" content="AsciiDoc 8.6.7" />
<LINK href="page-style.css" rel="stylesheet" type="text/css">
<title>A Guide to GEM Programming in C using AHCC</title>
</head>
<body class="article"><p><table width="100%"><td width="33.3%"><a href="Section-2.html">Section 2</a></td><td width="33.3%"><a href="index.html">Contents</a></td><td width="33.3%"><a href="Section-4.html">Section 4</a></td></table></p><div class="sect1">
<h2 id="_overview_of_a_gem_program">3. Overview of a GEM Program</h2>
<div class="sectionbody">
<div class="paragraph">
<p>If you are familiar with GUI programming on a more modern computer, kindly
forget everything you know.  Programming in GEM is primitive.  With a modern
toolkit, you have many kinds of widgets, from buttons and spinboxes, to
complete text editors supporting copy and paste and scrollable display areas.
Widgets can be combined with layout managers into complex arrangements.
Multi-threading can be used to keep the display refreshed while your program
is busy computing some results.</p>
</div>
<div class="paragraph">
<p>None of this is available in GEM.</p>
</div>
<div class="paragraph">
<p>In GEM there are, essentially, three elements: menus, dialog boxes (which can
contain several &#34;widgets&#34;, such as buttons or text fields) and windows.  The
part that will take most of our time is learning to handle the window: a window
is the part of the screen in which your application has free reign.  You have
complete control over the contents of the window.  You decide what gets drawn
there, how what you draw there responds to the user moving the sliders around,
and you must also ensure what you draw in the window remains there (and only
there) as your window interacts with the windows of other applications (in
MINT) or desk accessories.</p>
</div>
<div class="sect2">
<h3 id="_outline_of_material">3.1. Outline of Material</h3>
<div class="paragraph">
<p>The following sections progressively build up a GEM program to illustrate
window handling, resulting in a program that responds to all the expected
window events, has sliders, and co-exists happily with other windows,
preserving its own contents, and not disturbing the neighbours.  Each
successive version is provided complete in the source code for this guide, and
can be compiled using AHCC.</p>
</div>
<div class="paragraph">
<p>After getting to grips with windows, we move onto an easier aspect of GEM
programming: menus.  Menus are defined in the program’s RSC file, and all we
need to do is respond to a message indicating they have been selected.
We will also look at how to respond to keyboard commands, so we can use keyboard
shortcuts to menu items.</p>
</div>
<div class="paragraph">
<p>We then move on to dialog boxes.  The main issue with
dialog boxes is displaying them, and then retrieving any data the user has
input. Dialog boxes are the only place you find widgets such as buttons, text
fields and labels (unless you are using an advanced AES permitting toolbars).
Again, dialog boxes are defined in the program’s RSC file.</p>
</div>
<div class="admonitionblock note">
<table>
<tbody><tr>
<td class="icon">
<div class="title">Note</div>
</td>
<td class="content">
Although there is a lot of code required for managing GEM, with a little
discipline it is possible to preserve most of this code between projects.  The
examples here follow my own practice in managing this.  I shall try to make
clear which parts of the examples are GEM requirements, and which are my own
suggestions.
</td>
</tr>
</tbody></table>
</div>
<div class="paragraph">
<p>The different components of a GEM window:</p>
</div>
<div class="imageblock">
<div class="content">
<img src="images/info.jpg" alt="info" width="300"/>
</div>
</div>
<div class="paragraph">
<p>A GEM program typically does the following:</p>
</div>
<div class="olist arabic">
<ol class="arabic">
<li>
<p>initialise the application</p>
</li>
<li>
<p>open a VDI screen, and obtain a handle to access it</p>
</li>
<li>
<p>optionally open an RSC file and set up the menu</p>
</li>
<li>
<p>open an initial window or windows</p>
</li>
<li>
<p>enter the event loop, responding to all window / other events until
the application is quit</p>
</li>
<li>
<p>free any resources</p>
</li>
<li>
<p>free the VDI screen</p>
</li>
<li>
<p>exit the application</p>
</li>
</ol>
</div>
</div>
<div class="sect2">
<h3 id="_some_gem_terms">3.2. Some GEM Terms</h3>
<div class="paragraph">
<p>GEM comes with some terminology of its own, which may not be familiar to
the novice GEM programmer.</p>
</div>
<div class="dlist">
<dl>
<dt class="hdlist1">AES</dt>
<dd>
<p>the part of the operating system which handles the windows, dialogs,
menus, mouse etc.</p>
</dd>
<dt class="hdlist1">event</dt>
<dd>
<p>is how GEM informs our program that the user has clicked or dragged
on a window widget or menu item, or that we must redraw the display in a window.</p>
</dd>
<dt class="hdlist1">handle</dt>
<dd>
<p>is a number assigned to identify a window or application.  Your
application may have several windows open, and will need to identify which
window the user is attempting to interact with.  The window handle is used
as a unique reference for each window in your application.  Similarly,
there may be several applications running at once, and each application is
given a unique application handle to identify it.</p>
</dd>
<dt class="hdlist1">message</dt>
<dd>
<p>the information sent to our application about an event.  For example,
a redraw event would have information about the new window dimensions.</p>
</dd>
<dt class="hdlist1">RSC file</dt>
<dd>
<p>is a file which accompanies our GEM program, and contains
definitions of the menus and any user-defined dialogs which our program
requires.  The RSC file is created in an external program, such as
ResourceMaster.  The advantage of using RSC files is that it is easy
to distribute versions of the RSC for different languages, making our
GEM programs international.</p>
</dd>
<dt class="hdlist1">VDI</dt>
<dd>
<p>the part of the operating system responsible for drawing graphics
and text on the screen.</p>
</dd>
<dt class="hdlist1">virtual workstation</dt>
<dd>
<p>is the name given to the screen display.  When a GEM
program starts, it is assigned a handle to access the screen, which we call the
<code>app_handle</code>.  In theory, workstations may be created on other screens and/or
devices, but in practice this will always be the screen.</p>
</dd>
<dt class="hdlist1">widgets</dt>
<dd>
<p>are the components of a GEM window or dialog which we can click or
interact with, usually by clicking or dragging with the mouse.  For example,
the close button and sliders on a window are widgets, as is a button in a
dialog.</p>
</dd>
</dl>
</div>
</div>
</div>
</div></body>
</div>
