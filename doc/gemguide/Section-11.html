<div class='fossil-doc' data-title='Gem Guide'>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
    "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<head>
<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="generator" content="AsciiDoc 8.6.7" />
<LINK href="page-style.css" rel="stylesheet" type="text/css">
<title>A Guide to GEM Programming in C using AHCC</title>
</head>
<body class="article"><p><table width="100%"><td width="33.3%"><a href="Section-10.html">Section 10</a></td><td width="33.3%"><a href="index.html">Contents</a></td><td width="33.3%"><a href="Section-12.html">Section 12</a></td></table></p><div class="sect1">
<h2 id="_menus">11. Menus</h2>
<div class="sectionbody">
<div class="paragraph">
<p>The menu bar is a convenient way of interacting with our programs.  A menu
bar is defined within an RSC file, which is a file containing all the
resources, such as menu bars and dialogs, used by our program.  The RSC
file is created by an external program: there are several available, but
I use Resource Master 3.65.  When started, our program must load in its
RSC file, and access the required information to show menu bars and dialogs.</p>
</div>
<div class="paragraph">
<p>Menus, in GEM, typically appear at the top of the screen.  The menu bar
can contain several menus.  Each menu has a title, and can contain a number
of menu items.  Each menu item may have an associated key command.  Also,
menu items may be disabled (meaning they cannot be selected), or have a
check mark showing.  Menus can also contain separator items, which are
horizontal lines to divide the menu into groups.  The image below
illustrates these elements:</p>
</div>
<div class="imageblock">
<div class="content">
<img src="images/menu.png" alt="menu" width="200"/>
</div>
</div>
<div class="paragraph">
<p>Menus also provide access to the desk accessories.  The convention is that
the first menu item, on the left, refers to the program name, and contains
a single item, which opens a dialog showing information about the program.
This first menu item is followed by a separator, and then a list of desk
accessories (or &#34;Clients&#34; in Mint).</p>
</div>
<div class="paragraph">
<p>Interactions with the menu are simple: when a menu item is clicked on with
the mouse, a message is sent to the program indicating that the menu has been
used.  This message contains information about the menu item that was clicked.</p>
</div>
<div class="paragraph">
<p>Keyboard shortcuts are also an integral part of using menus. In order to handle
these, we need to discuss more about event handling, which we do in the
following section.</p>
</div>
<div class="sect2">
<h3 id="_the_rsc_and_rsh_files">11.1. The RSC and rsh files</h3>
<div class="paragraph">
<p>Before our program can use a menu, we need to build the menu in a resource
construction program (such as Resource Master) and save it as a RSC file.
Our program also needs a way of associating the clicked on menu item with
the information sent in the message event.  This information is contained
in a set of symbol definitions, saved by the resource construction program
in a .rsh file (or equivalent).</p>
</div>
<div class="admonitionblock tip">
<table>
<tbody><tr>
<td class="icon">
<div class="title">Tip</div>
</td>
<td class="content">
It is standard practice to name our .RSC file using the program name.
Do <em>not</em> use your program name for a .C source file.  For example, if your
program is &#39;sokoban&#39;, we will have &#39;sokoban.rsc&#39;.  Do <em>not</em> name one of
your source files &#39;sokoban.c&#39;.  ResourceMaster, for example, will export
a .c file if you choose to build a desk accessory, and this will overwrite
any source files of the same name.  For safety, only use your program name
as the .PRG name in your project file, and as the .RSC name.
</td>
</tr>
</tbody></table>
</div>
<div class="paragraph">
<p>As we create each menu item, we associate with that menu item a symbol,
used to name it.  When we export the C header file, we get a file containing
symbol definitions for each menu item.  For Resource Master, the output looks
something like the following.  Note that the symbol &#39;ABOUT&#39; attached to the
&#39;About&#39; menu item has been prefixed with the symbol for the main menu itself.</p>
</div>
<div class="listingblock">
<div class="content">
<pre> /*  Resource C-Header-file v1.95 for ResourceMaster v2.06&amp;up by ARDISOFT  */

#define MAIN_MENU 0  /* menu */					<b class="conum">(1)</b>
#define MAIN_MENU_ABOUT 9  /* STRING in tree MAIN_MENU */	<b class="conum">(2)</b></pre>
</div>
</div>
<div class="colist arabic">
<ol>
<li>
<p>The symbol for the main menu itself.</p>
</li>
<li>
<p>The symbol for the &#39;About&#39; item on the main menu.</p>
</li>
</ol>
</div>
<div class="paragraph">
<p>Before we can use the RSC file, we need to include the .rsh file in our
source code.  We do this by adding a line in &#34;windows.h&#34;, for example:</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="rouge highlight"><code data-lang="c"><span class="cp">#include &#34;version7.rsh&#34;</span></code></pre>
</div>
</div>
</div>
<div class="sect2">
<h3 id="_showing_a_menu">11.2. Showing a Menu</h3>
<div class="paragraph">
<p>Before we can show the menu, our program must open the .RSC file.  We must
check the file has opened successfully before proceeding.  Having done
this, we can locate the menu bar and show it.  Once our program has
finished, we should remove the menu bar.  Our <code>start_program</code> function is
accordingly modified, as follows:</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="rouge highlight"><code data-lang="c"><span class="kt">void</span> <span class="nf">start_program</span> <span class="p">(</span><span class="kt">void</span><span class="p">)</span> <span class="p">{</span>
	<span class="cm">/* DECLARE LOCAL VARIABLES */</span>

	<span class="k">if</span> <span class="p">(</span><span class="o">!</span><span class="n">rsrc_load</span> <span class="p">(</span><span class="s">&#34;VERSION7.rsc&#34;</span><span class="p">))</span> <span class="p">{</span>			// <b class="conum">(1)</b>
		<span class="n">form_alert</span> <span class="p">(</span><span class="mi">1</span><span class="p">,</span> <span class="s">&#34;[1][version7 .rsc file missing!][OK]&#34;</span><span class="p">);</span>
	<span class="p">}</span> <span class="k">else</span> <span class="p">{</span>
		<span class="n">OBJECT</span> <span class="o">*</span> <span class="n">menu_addr</span><span class="p">;</span>				// <b class="conum">(2)</b>

		<span class="cm">/* 1. install the menu bar */</span>
		<span class="n">rsrc_gaddr</span> <span class="p">(</span><span class="n">R_TREE</span><span class="p">,</span> <span class="n">MAIN_MENU</span><span class="p">,</span> <span class="o">&amp;</span><span class="n">menu_addr</span><span class="p">);</span>	// <b class="conum">(3)</b>
		<span class="n">menu_bar</span> <span class="p">(</span><span class="n">menu_addr</span><span class="p">,</span> <span class="nb">true</span><span class="p">);</span>			// <b class="conum">(4)</b>

		<span class="cm">/* 2. OPEN WINDOWS ETC */</span>

		<span class="cm">/* 3. process events for our window */</span>
		<span class="n">event_loop</span> <span class="p">(</span><span class="n">menu_addr</span><span class="p">,</span> <span class="o">&amp;</span><span class="n">wd1</span><span class="p">);</span>			// <b class="conum">(5)</b>

		<span class="cm">/* 4. remove the menu bar */</span>
		<span class="n">menu_bar</span> <span class="p">(</span><span class="n">menu_addr</span><span class="p">,</span> <span class="nb">false</span><span class="p">);</span>			// <b class="conum">(6)</b>

		<span class="cm">/* 5. CLOSE WINDOWS AND CLEAN UP */</span>
	<span class="p">}</span>
<span class="p">}</span></code></pre>
</div>
</div>
<div class="colist arabic">
<ol>
<li>
<p>Attempt to load the RSC file.  If it fails to load, display a dialog,
and then abort the program.</p>
</li>
<li>
<p>Create a local variable to hold the address of the menu.</p>
</li>
<li>
<p>Retrieve the menu from the RSC data.  We provide the name of the
menu <code>MAIN_MENU</code>, and the variable to store the menu’s address.</p>
</li>
<li>
<p>Display the menu bar for our application.</p>
</li>
<li>
<p><code>menu_addr</code> is also passed to the <code>event_loop</code>.</p>
</li>
<li>
<p>Remove the menu bar, which frees up its resources.</p>
</li>
</ol>
</div>
<div class="admonitionblock note">
<table>
<tbody><tr>
<td class="icon">
<div class="title">Note</div>
</td>
<td class="content">
<code>rsrc_load</code> likes the filename in capital letters.
</td>
</tr>
</tbody></table>
</div>
</div>
<div class="sect2">
<h3 id="_responding_to_menu_events">11.3. Responding to Menu Events</h3>
<div class="paragraph">
<p>Menu events are passed to our program using the <code>MN_SELECTED</code> event type.
<code>msg_buf[4]</code> contains the reference to the actual menu selected.  These
references are defined in the .rsh file.</p>
</div>
<div class="paragraph">
<p>Within the <code>event_loop</code> function switch function, we add the following case
to respond to menu events:</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="rouge highlight"><code data-lang="c">	<span class="k">case</span> <span class="n">MN_SELECTED</span><span class="p">:</span> <span class="cm">/* menu selection */</span>
        	<span class="n">do_menu</span> <span class="p">(</span><span class="n">wd</span><span class="p">,</span> <span class="n">msg_buf</span><span class="p">[</span><span class="mi">4</span><span class="p">]);</span> 			// <b class="conum">(1)</b>
		<span class="cm">/* return menu to normal */</span>
        	<span class="n">menu_tnormal</span> <span class="p">(</span><span class="n">menu_addr</span><span class="p">,</span> <span class="n">msg_buf</span><span class="p">[</span><span class="mi">3</span><span class="p">],</span> <span class="nb">true</span><span class="p">);</span>	// <b class="conum">(2)</b>
		<span class="k">break</span><span class="p">;</span></code></pre>
</div>
</div>
<div class="colist arabic">
<ol>
<li>
<p>Call out to <code>do_menu</code> with the selected menu item.</p>
</li>
<li>
<p>Once the menu event has been dealt with, return the menu display to normal.</p>
</li>
</ol>
</div>
<div class="paragraph">
<p>Notice how GEM highlights the selected menu item whilst the event is carried
out.  Our program is responsible for returning the item to normal once it has
finished.</p>
</div>
<div class="paragraph">
<p>The <code>do_menu</code> function simply dispatches to a function to deal with each
of the possible menu items.</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="rouge highlight"><code data-lang="c"><span class="kt">void</span> <span class="nf">do_menu</span> <span class="p">(</span><span class="k">struct</span> <span class="n">win_data</span> <span class="o">*</span> <span class="n">wd</span><span class="p">,</span> <span class="kt">int</span> <span class="n">menu_item</span><span class="p">)</span> <span class="p">{</span>
	<span class="k">switch</span> <span class="p">(</span><span class="n">menu_item</span><span class="p">)</span> <span class="p">{</span>

		<span class="k">case</span> <span class="n">MAIN_MENU_ABOUT</span><span class="p">:</span>		// <b class="conum">(1)</b>
			<span class="n">do_about</span> <span class="p">();</span>
			<span class="k">break</span><span class="p">;</span>
	<span class="p">}</span>
<span class="p">}</span></code></pre>
</div>
</div>
<div class="colist arabic">
<ol>
<li>
<p>The case statement uses the name for the menu item as defined in the
RSC file.</p>
</li>
</ol>
</div>
<div class="paragraph">
<p>The <code>do_about</code> function displays a simple information dialog - we discuss
dialogs in a later section.</p>
</div>
<div class="paragraph">
<p>How about the QUIT option?  When the user selects quit, we don’t have to
do anything except exit the event loop.  This is easily done in the <code>while</code>
condition:</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="rouge highlight"><code data-lang="c">	<span class="err">}</span> <span class="k">while</span> <span class="p">(</span><span class="o">!</span><span class="p">(</span><span class="n">msg_buf</span><span class="p">[</span><span class="mi">0</span><span class="p">]</span> <span class="o">==</span> <span class="n">MN_SELECTED</span> <span class="o">&amp;&amp;</span> <span class="n">msg_buf</span><span class="p">[</span><span class="mi">4</span><span class="p">]</span> <span class="o">==</span> <span class="n">MAIN_MENU_QUIT</span><span class="p">));</span> // <b class="conum">(1)</b></code></pre>
</div>
</div>
<div class="colist arabic">
<ol>
<li>
<p>Loop terminates when it has a <code>MN_SELECTED</code> event which is &#39;Quit&#39;.</p>
</li>
</ol>
</div>
</div>
<div class="sect2">
<h3 id="_controlling_the_menu_items">11.4. Controlling the Menu Items</h3>
<div class="paragraph">
<p>An individual menu item can be enabled or disabled.  A disabled menu item
cannot be selected, and is shown greyed out on the menu bar.  Whether a
menu item is enabled or not is controlled using:</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="rouge highlight"><code data-lang="c"><span class="n">menu_ienable</span> <span class="p">(</span><span class="n">menu_addr</span><span class="p">,</span> <span class="n">OBJECT</span><span class="p">,</span> <span class="n">flag</span><span class="p">);</span></code></pre>
</div>
</div>
<div class="paragraph">
<p>where</p>
</div>
<div class="ulist">
<ul>
<li>
<p><code>menu_addr</code> is the address of the menu;</p>
</li>
<li>
<p><code>OBJECT</code> is the reference to the menu item; and</p>
</li>
<li>
<p><code>flag</code> is true to enable or false to disable the menu item.</p>
</li>
</ul>
</div>
<div class="paragraph">
<p>Each menu item can also show an optional check (or &#39;tick&#39;) mark
beside it.  The code to control this takes the same parameters as
<code>menu_ienable</code>:</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="rouge highlight"><code data-lang="c"><span class="n">menu_icheck</span> <span class="p">(</span><span class="n">menu_addr</span><span class="p">,</span> <span class="n">OBJECT</span><span class="p">,</span> <span class="n">flag</span><span class="p">);</span></code></pre>
</div>
</div>
<div class="paragraph">
<p>Finally, it is possible to change the text showing on a menu item,
with the call:</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="rouge highlight"><code data-lang="c"><span class="n">menu_text</span> <span class="p">(</span><span class="n">menu_addr</span><span class="p">,</span> <span class="n">OBJECT</span><span class="p">,</span> <span class="n">str</span><span class="p">);</span> // <b class="conum">(1)</b></code></pre>
</div>
</div>
<div class="colist arabic">
<ol>
<li>
<p><code>str</code> must be a statically allocated string.  Remember to keep
two spaces at the start of the string.</p>
</li>
</ol>
</div>
<div class="paragraph">
<p>The sample program does not have a good reason to use checked menu items
or to disable items, so there is a menu provided just to try out these
options.  One menu item controls whether the other is enabled or not, and
shows a check mark if it is enabled.  For good measure, the altered menu item
also has its text changed.  We store the state for this as
a global variable, <code>menu_state</code>; in a real application the state will likely
depend on the top-most window’s contents, and so be stored within <code>win_data</code>.</p>
</div>
<div class="paragraph">
<p>The action is handled directly in <code>do_menu</code>:</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="rouge highlight"><code data-lang="c">	<span class="k">case</span> <span class="n">MAIN_MENU_SWITCH</span><span class="p">:</span>
		<span class="n">menu_state</span> <span class="o">=</span> <span class="o">!</span><span class="n">menu_state</span><span class="p">;</span>				// <b class="conum">(1)</b>
		<span class="n">menu_icheck</span> <span class="p">(</span><span class="n">menu_addr</span><span class="p">,</span> <span class="n">MAIN_MENU_SWITCH</span><span class="p">,</span> <span class="n">menu_state</span><span class="p">);</span>	// <b class="conum">(2)</b>
		<span class="n">menu_ienable</span> <span class="p">(</span><span class="n">menu_addr</span><span class="p">,</span> <span class="n">MAIN_MENU_DUMMY</span><span class="p">,</span> <span class="n">menu_state</span><span class="p">);</span>	// <b class="conum">(3)</b>
		<span class="n">menu_text</span> <span class="p">(</span><span class="n">menu_addr</span><span class="p">,</span> <span class="n">MAIN_MENU_DUMMY</span><span class="p">,</span> 			// <b class="conum">(4)</b>
			   <span class="p">(</span><span class="n">menu_state</span> <span class="o">?</span> <span class="s">&#34;  Enabled&#34;</span> <span class="o">:</span> <span class="s">&#34;   Disabled&#34;</span><span class="p">));</span>
		<span class="k">break</span><span class="p">;</span></code></pre>
</div>
</div>
<div class="colist arabic">
<ol>
<li>
<p>Invert the state.</p>
</li>
<li>
<p>Set the status of the SWITCH menu item’s check sign.</p>
</li>
<li>
<p>Set the enabled/disabled status of the DUMMY menu item.</p>
</li>
<li>
<p>Change the text on the DUMMY menu item.</p>
</li>
</ol>
</div>
</div>
</div>
</div></body>
</div>
