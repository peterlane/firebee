<div class='fossil-doc' data-title='Gem Guide'>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
    "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<head>
<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="generator" content="AsciiDoc 8.6.7" />
<LINK href="page-style.css" rel="stylesheet" type="text/css">
<title>A Guide to GEM Programming in C using AHCC</title>
</head>
<body class="article"><p><table width="100%"><td width="33.3%"><a href="Section-11.html">Section 11</a></td><td width="33.3%"><a href="index.html">Contents</a></td><td width="33.3%"><a href="Section-13.html">Section 13</a></td></table></p><div class="sect1">
<h2 id="_events">12. Events</h2>
<div class="sectionbody">
<div class="paragraph">
<p>This is an appropriate time to consider event handling in more detail, so
we can extend the range of events we can respond to.  For example, in the
last section we considered menus.
Important menu items should contain keyboard shortcuts, for users who prefer
not to take their hands from the keyboard.  Keyboard events are sent to our
program just like other events, but we must listen for them.  To do so, we
must change our event loop to handle multiple event types.</p>
</div>
<div class="paragraph">
<p>Any GEM program may produce one or more of a range of different event types.
These include:</p>
</div>
<div class="ulist">
<ul>
<li>
<p><code>MU_MESAG</code>: AES messages for the window and menu events (as we have handled so far)</p>
</li>
<li>
<p>Mouse events: information on the mouse, there are three of these</p>
<div class="ulist">
<ul>
<li>
<p><code>MU_M1</code>: When mouse enters or leaves region 1</p>
</li>
<li>
<p><code>MU_M2</code>: When mouse enters or leaves region 2</p>
</li>
<li>
<p><code>MU_BUTTON</code>: Information on button clicks</p>
</li>
</ul>
</div>
</li>
<li>
<p><code>MU_TIMER</code>: Timer events, an event triggered at regular time intervals</p>
</li>
<li>
<p><code>MU_KEYBD</code>: Keyboard events, triggered when the user types on the keyboard</p>
</li>
</ul>
</div>
<div class="paragraph">
<p>Although GEM provides separate event listeners for these different types, in
any real GEM program multiple event types will be needed.  For example, any
GEM program will need to respond to AES messages about the windows and menus,
mouse events and the keyboard, at a minimum; to do this, we need to use a
multiple-event listener.</p>
</div>
<div class="paragraph">
<p>There are two ways to handle multiple event types.  There is the <em>traditional</em>
way, which uses a call to <code>evnt_multi</code>.  This requires setting up several
internal variables to store any required return values.  AHCC offers an
alternative way, which uses a call to <code>EvntMulti</code>, using a convenient <code>struct</code>
to hold all input and return values.  The advantage of <code>evnt_multi</code> is
conformance to previous practice.  The advantage of <code>EvntMulti</code> is a simpler
calling routine; the AHCC description also claims improved performance.  We
shall describe both ways.</p>
</div>
<div class="sect2">
<h3 id="_traditional_evnt_multi">12.1. Traditional evnt_multi</h3>
<div class="paragraph">
<p>The traditional <code>evnt_multi</code> function is an extension of the <code>evnt_mesag</code>
we have been using so far.  It offers the advantage of being documented in
the Atari literature, and will be familiar to most programmers.  However,
it does require care in defining and presenting different reference variables
to hold various return values.</p>
</div>
<div class="paragraph">
<p>The call to <code>evnt_multi</code> looks as follows:</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="rouge highlight"><code data-lang="c"><span class="kt">int</span> <span class="nf">evnt_multi</span><span class="p">(</span> <span class="kt">int</span> <span class="n">ev_mflags</span><span class="p">,</span> <span class="kt">int</span> <span class="n">ev_mbclicks</span><span class="p">,</span> <span class="kt">int</span> <span class="n">ev_mbmask</span><span class="p">,</span>
                <span class="kt">int</span> <span class="n">ev_mbstate</span><span class="p">,</span> <span class="kt">int</span> <span class="n">ev_mm1flags</span><span class="p">,</span> <span class="kt">int</span> <span class="n">ev_mm1x</span><span class="p">,</span>
                <span class="kt">int</span> <span class="n">ev_mm1y</span><span class="p">,</span> <span class="kt">int</span> <span class="n">ev_mm1width</span><span class="p">,</span> <span class="kt">int</span> <span class="n">ev_mm1height</span><span class="p">,</span>
                <span class="kt">int</span> <span class="n">ev_mm2flags</span><span class="p">,</span> <span class="kt">int</span> <span class="n">ev_mm2x</span><span class="p">,</span> <span class="kt">int</span> <span class="n">ev_mm2y</span><span class="p">,</span>
                <span class="kt">int</span> <span class="n">ev_mm2width</span><span class="p">,</span> <span class="kt">int</span> <span class="n">ev_mm2height</span><span class="p">,</span>
                <span class="kt">int</span> <span class="o">*</span><span class="n">ev_mmgpbuff</span><span class="p">,</span> <span class="kt">int</span> <span class="n">ev_mtlocount</span><span class="p">,</span>
                <span class="kt">int</span> <span class="n">ev_mthicount</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">ev_mmox</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">ev_mmoy</span><span class="p">,</span>
                <span class="kt">int</span> <span class="o">*</span><span class="n">ev_mmbutton</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">ev_mmokstate</span><span class="p">,</span>
                <span class="kt">int</span> <span class="o">*</span><span class="n">ev_mkreturn</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">ev_mbreturn</span> <span class="p">);</span></code></pre>
</div>
</div>
<div class="paragraph">
<p>Many of these variables define values describing the kinds of events
to listen for.  <code>ev_mflags</code> indicates the event types, such as mouse
and keyboard.  <code>ev_mbclicks</code> describe the number of clicks that will
trigger an event, etc.</p>
</div>
<div class="paragraph">
<p>The reference variables (pointers to ints) are used for return values.
For example, <code>ev_mkreturn</code> will hold the code of a pressed key.
<code>ev_mmgpbuff</code> is a pointer to the message buffer for AES messages.</p>
</div>
<div class="paragraph">
<p>The following is a complete list:</p>
</div>
<div class="ulist">
<ul>
<li>
<p>ev_mflags: flags indicating which events to listen for</p>
</li>
<li>
<p>ev_mbclicks: number of mouse clicks to listen for</p>
</li>
<li>
<p>ev_mbmask: button mask (for which button)</p>
</li>
<li>
<p>ev_mbstate: button state (0 for up, 1 for down)</p>
</li>
<li>
<p>ev_mm1flags: 1 for leave, 0 for enter region 1</p>
</li>
<li>
<p>ev_mm1x: x coordinate of region 1</p>
</li>
<li>
<p>ev_mm1y: y coordinate of region 1</p>
</li>
<li>
<p>ev_mm1width: width of region 1</p>
</li>
<li>
<p>ev_mm1height: height of region 1</p>
</li>
<li>
<p>ev_mm2flags: 1 for leave, 0 for enter region 2</p>
</li>
<li>
<p>ev_mm2x: x coordinate of region 2</p>
</li>
<li>
<p>ev_mm2y: y coordinate of region 2</p>
</li>
<li>
<p>ev_mm2width: width of region 2</p>
</li>
<li>
<p>ev_mm2height: height of region 2</p>
</li>
<li>
<p>ev_mtlocount: low count for timer event</p>
</li>
<li>
<p>ev_mthicount: high count for timer event (pair is a long value for time in ms)</p>
</li>
<li>
<p>ev_mmox: mouse x coordinate</p>
</li>
<li>
<p>ev_mmoy: mouse y coordinate</p>
</li>
<li>
<p>ev_mmbutton: button state</p>
</li>
<li>
<p>ev_mmokstate: key state (bit 0 right-shift, 1 left-shift, 2 ctrl, 3 alt)</p>
</li>
<li>
<p>ev_mkreturn: holds the code of the pressed key</p>
</li>
<li>
<p>ev_mbreturn: number of clicks</p>
</li>
<li>
<p>ev_mmgpbuf: this is an array of 8 ints, which we have called <code>msg_buf</code></p>
</li>
</ul>
</div>
<div class="paragraph">
<p>The return value from <code>evnt_multi</code> gives the actual event type that occurred.
For example, if we wish to listen for an AES message, mouse click or keyboard
event, we would call <code>evnt_multi</code> with the flags:</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="rouge highlight"><code data-lang="c"><span class="n">result</span> <span class="o">=</span> <span class="n">evnt_multi</span> <span class="p">(</span><span class="n">MU_MESAG</span> <span class="o">|</span> <span class="n">MU_KEYBD</span> <span class="o">|</span> <span class="n">MU_BUTTON</span><span class="p">,</span> <span class="p">...);</span></code></pre>
</div>
</div>
<div class="paragraph">
<p><code>result</code> would then hold one of the flag values, depending on which event
type occurred.</p>
</div>
<div class="paragraph">
<p>The following excerpt from version 7 of our sample program illustrates how
<code>evnt_multi</code> is used.  In this version we listen for the standard AES messages
regarding the menus and window events, but additionally look out for a
keyboard event:</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="rouge highlight"><code data-lang="c"><span class="kt">void</span> <span class="nf">event_loop</span> <span class="p">(</span><span class="n">OBJECT</span> <span class="o">*</span> <span class="n">menu_addr</span><span class="p">,</span> <span class="k">struct</span> <span class="n">win_data</span> <span class="o">*</span> <span class="n">wd</span><span class="p">)</span> <span class="p">{</span>
	<span class="kt">int</span> <span class="n">msg_buf</span><span class="p">[</span><span class="mi">8</span><span class="p">];</span>
	<span class="kt">int</span> <span class="n">dum</span><span class="p">,</span> <span class="n">key</span><span class="p">,</span> <span class="n">event_type</span><span class="p">;</span>					      // <b class="conum">(1)</b>

	<span class="k">do</span> <span class="p">{</span>
		<span class="n">event_type</span> <span class="o">=</span> <span class="n">evnt_multi</span> <span class="p">(</span><span class="n">MU_MESAG</span> <span class="o">|</span> <span class="n">MU_KEYBD</span><span class="p">,</span> <span class="mi">0</span><span class="p">,</span><span class="mi">0</span><span class="p">,</span><span class="mi">0</span><span class="p">,</span><span class="mi">0</span><span class="p">,</span><span class="mi">0</span><span class="p">,</span>
                                         <span class="mi">0</span><span class="p">,</span><span class="mi">0</span><span class="p">,</span><span class="mi">0</span><span class="p">,</span><span class="mi">0</span><span class="p">,</span><span class="mi">0</span><span class="p">,</span><span class="mi">0</span><span class="p">,</span><span class="mi">0</span><span class="p">,</span><span class="mi">0</span><span class="p">,</span><span class="n">msg_buf</span><span class="p">,</span><span class="mi">0</span><span class="p">,</span><span class="mi">0</span><span class="p">,</span>
					 <span class="o">&amp;</span><span class="n">dum</span><span class="p">,</span> <span class="o">&amp;</span><span class="n">dum</span><span class="p">,</span> <span class="o">&amp;</span><span class="n">dum</span><span class="p">,</span> <span class="o">&amp;</span><span class="n">dum</span><span class="p">,</span> <span class="o">&amp;</span><span class="n">key</span><span class="p">,</span> <span class="o">&amp;</span><span class="n">dum</span><span class="p">);</span> // <b class="conum">(2)</b>

		<span class="cm">/* -- check for and handle keyboard events */</span>
		<span class="k">if</span> <span class="p">(</span><span class="n">event_type</span> <span class="o">&amp;</span> <span class="n">MU_KEYBD</span><span class="p">)</span> <span class="p">{</span>				      // <b class="conum">(3)</b>
			<span class="k">if</span> <span class="p">(</span><span class="n">key</span> <span class="o">==</span> <span class="mh">0x1011</span><span class="p">)</span> <span class="p">{</span> <span class="cm">/* code for ctrl-Q  */</span>              <b class="conum">(4)</b>
				<span class="k">break</span><span class="p">;</span> <span class="cm">/* exit the do-while loop */</span>
			<span class="p">}</span>
		<span class="p">}</span>

		<span class="cm">/* -- check for and handle menu events */</span>
		<span class="k">if</span> <span class="p">(</span><span class="n">event_type</span> <span class="o">&amp;</span> <span class="n">MU_MESAG</span><span class="p">)</span> <span class="p">{</span>
			<span class="k">switch</span> <span class="p">(</span><span class="n">msg_buf</span><span class="p">[</span><span class="mi">0</span><span class="p">])</span> <span class="p">{</span>

				<span class="k">case</span> <span class="n">MN_SELECTED</span><span class="p">:</span> <span class="cm">/* menu selection */</span>
					<span class="n">do_menu</span> <span class="p">(</span><span class="n">menu_addr</span><span class="p">,</span> <span class="n">wd</span><span class="p">,</span> <span class="n">msg_buf</span><span class="p">[</span><span class="mi">4</span><span class="p">]);</span>
					<span class="cm">/* return menu to normal */</span>
					<span class="n">menu_tnormal</span> <span class="p">(</span><span class="n">menu_addr</span><span class="p">,</span> <span class="n">msg_buf</span><span class="p">[</span><span class="mi">3</span><span class="p">],</span> <span class="nb">true</span><span class="p">);</span>
					<span class="k">break</span><span class="p">;</span>

				<span class="c1">// REMAINING CASE STATEMENTS</span>

			<span class="p">}</span>
		<span class="p">}</span>
	<span class="p">}</span> <span class="k">while</span> <span class="p">((</span><span class="n">MN_SELECTED</span> <span class="o">!=</span> <span class="n">msg_buf</span><span class="p">[</span><span class="mi">0</span><span class="p">])</span> <span class="o">||</span> <span class="p">(</span><span class="n">MAIN_MENU_QUIT</span> <span class="o">!=</span> <span class="n">msg_buf</span><span class="p">[</span><span class="mi">4</span><span class="p">]));</span>
<span class="p">}</span></code></pre>
</div>
</div>
<div class="colist arabic">
<ol>
<li>
<p>Create some variables to hold the values created in <code>evnt_multi</code>.
<code>dum</code> is used for those slots we do not need.</p>
</li>
<li>
<p>Call <code>evnt_multi</code> with flags for the events we are listening for and
references to the variables to hold results.</p>
</li>
<li>
<p>Check <code>event_type</code> for the actual event that occurred.</p>
</li>
<li>
<p>Use the variables to locate results relevant to each event type.</p>
</li>
</ol>
</div>
</div>
<div class="sect2">
<h3 id="_ahcc_evntmulti">12.2. AHCC EvntMulti</h3>
<div class="paragraph">
<p>AHCC’s custom <code>EvntMulti</code> offers the advantage of a simpler calling routine.
Instead of defining separate variables for the possible return values, we
simply define one instance of <code>EVENT</code>, and pass its address to <code>EvntMulti</code>.
The return values and the <code>msgbuf</code> are all then stored within our instance
of <code>EVENT</code>.  (The only small downside is that the slot names are not very
intuitive.)</p>
</div>
<div class="paragraph">
<p>The struct <code>EVENT</code> has a slot for each of the arguments to <code>evnt_multi</code>, as
discussed above (except that <code>ev_mbmask</code> is called <code>ev_bmask</code> and <code>ev_mmbutton</code>
is called <code>ev_mmobutton</code>).  Instead of passing the values by position in the
function call, input values are set and output values are stored in the
relevant slots.</p>
</div>
<div class="paragraph">
<p>We use <code>EvntMulti</code> just like <code>evnt_multi</code>.  First we set the input data for
the events we want to listen for, then we call <code>EvntMulti</code>, and use its
return value to decide which kind of event has occurred.  Information about
the event is stored in the relevant output slots of <code>EVENT</code>.</p>
</div>
<div class="paragraph">
<p>The following excerpt is from our example program, version 7.  It does the
same as was done using the <code>evnt_multi</code> call above:</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="rouge highlight"><code data-lang="c"><span class="kt">void</span> <span class="nf">event_loop</span> <span class="p">(</span><span class="n">OBJECT</span> <span class="o">*</span> <span class="n">menu_addr</span><span class="p">,</span> <span class="k">struct</span> <span class="n">win_data</span> <span class="o">*</span> <span class="n">wd</span><span class="p">)</span> <span class="p">{</span>
	<span class="n">EVENT</span> <span class="n">ev</span><span class="p">;</span>					// <b class="conum">(1)</b>

	<span class="n">ev</span><span class="p">.</span><span class="n">ev_mflags</span> <span class="o">=</span> <span class="n">MU_MESAG</span> <span class="o">|</span> <span class="n">MU_KEYBD</span><span class="p">;</span>		// <b class="conum">(2)</b>

	<span class="k">do</span> <span class="p">{</span>
		<span class="kt">int</span> <span class="n">event_type</span> <span class="o">=</span> <span class="n">EvntMulti</span> <span class="p">(</span><span class="o">&amp;</span><span class="n">ev</span><span class="p">);</span>	// <b class="conum">(3)</b>

		<span class="cm">/* -- check for and handle keyboard events */</span>
		<span class="k">if</span> <span class="p">(</span><span class="n">event_type</span> <span class="o">&amp;</span> <span class="n">MU_KEYBD</span><span class="p">)</span> <span class="p">{</span>
			<span class="k">if</span> <span class="p">(</span><span class="n">ev</span><span class="p">.</span><span class="n">ev_mkreturn</span> <span class="o">==</span> <span class="mh">0x1011</span><span class="p">)</span> <span class="p">{</span> <span class="cm">/* code for ctrl-Q */</span>
				<span class="k">break</span><span class="p">;</span> <span class="cm">/* exit the do-while loop */</span>
			<span class="p">}</span>
		<span class="p">}</span>

		<span class="cm">/* -- check for and handle menu events */</span>
		<span class="k">if</span> <span class="p">(</span><span class="n">event_type</span> <span class="o">&amp;</span> <span class="n">MU_MESAG</span><span class="p">)</span> <span class="p">{</span>		// <b class="conum">(4)</b>
			<span class="k">switch</span> <span class="p">(</span><span class="n">ev</span><span class="p">.</span><span class="n">ev_mmgpbuf</span><span class="p">[</span><span class="mi">0</span><span class="p">])</span> <span class="p">{</span>	// <b class="conum">(5)</b>

				<span class="k">case</span> <span class="n">MN_SELECTED</span><span class="p">:</span> <span class="cm">/* menu selection */</span>
					<span class="n">do_menu</span> <span class="p">(</span><span class="n">menu_addr</span><span class="p">,</span> <span class="n">wd</span><span class="p">,</span> <span class="n">ev</span><span class="p">.</span><span class="n">ev_mmgpbuf</span><span class="p">[</span><span class="mi">4</span><span class="p">]);</span>
					<span class="cm">/* return menu to normal */</span>
					<span class="n">menu_tnormal</span> <span class="p">(</span><span class="n">menu_addr</span><span class="p">,</span> <span class="n">ev</span><span class="p">.</span><span class="n">ev_mmgpbuf</span><span class="p">[</span><span class="mi">3</span><span class="p">],</span> <span class="nb">true</span><span class="p">);</span>
					<span class="k">break</span><span class="p">;</span>

				<span class="c1">// REMAINING CASES</span>
			<span class="p">}</span>
		<span class="p">}</span>
	<span class="p">}</span> <span class="k">while</span> <span class="p">((</span><span class="n">MN_SELECTED</span> <span class="o">!=</span> <span class="n">ev</span><span class="p">.</span><span class="n">ev_mmgpbuf</span><span class="p">[</span><span class="mi">0</span><span class="p">])</span> <span class="o">||</span>
                 <span class="p">(</span><span class="n">MAIN_MENU_QUIT</span> <span class="o">!=</span> <span class="n">ev</span><span class="p">.</span><span class="n">ev_mmgpbuf</span><span class="p">[</span><span class="mi">4</span><span class="p">]));</span>
<span class="p">}</span></code></pre>
</div>
</div>
<div class="colist arabic">
<ol>
<li>
<p>Create an instance of the <code>EVENT</code> struct.</p>
</li>
<li>
<p>Set the flags for the event types to listen for.</p>
</li>
<li>
<p>Make the call to <code>EvntMulti</code>.</p>
</li>
<li>
<p>Test the event type, just as with <code>evnt_multi</code>.</p>
</li>
<li>
<p>Slots in <code>ev</code> store the return values, like the variables used in <code>evnt_multi</code>.</p>
</li>
</ol>
</div>
</div>
<div class="sect2">
<h3 id="_sample_program_binary_clock">12.3. Sample Program: Binary Clock</h3>
<div class="paragraph">
<p>In the &#39;clock&#39; folder of the source code which accompanies this guide is a
simple clock program.  To be a little different, this displays clock times
in binary.  (This program looks best in colour.)</p>
</div>
<div class="imageblock">
<div class="content">
<img src="images/binary.jpg" alt="binary" width="150"/>
</div>
</div>
<div class="paragraph">
<p>The program illustrates the use of the timer event, along with the usual
close/top/move/redraw messages for a GEM window.  The program simply redraws
its display after each second.</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="rouge highlight"><code data-lang="c"><span class="kt">void</span> <span class="nf">event_loop</span> <span class="p">(</span><span class="k">struct</span> <span class="n">win_data</span> <span class="o">*</span> <span class="n">wd</span><span class="p">)</span> <span class="p">{</span>
	<span class="n">EVENT</span> <span class="n">ev</span><span class="p">;</span>

	<span class="cm">/* listen for AES events and timer events */</span>
	<span class="n">ev</span><span class="p">.</span><span class="n">ev_mflags</span> <span class="o">=</span> <span class="n">MU_MESAG</span> <span class="o">|</span> <span class="n">MU_TIMER</span><span class="p">;</span>		// <b class="conum">(1)</b>

	<span class="cm">/* timer should fire every second */</span> 		   <b class="conum">(2)</b>
	<span class="n">ev</span><span class="p">.</span><span class="n">ev_mtlocount</span> <span class="o">=</span> <span class="mi">1000</span><span class="p">;</span>
	<span class="n">ev</span><span class="p">.</span><span class="n">ev_mthicount</span> <span class="o">=</span> <span class="mi">0</span><span class="p">;</span>

	<span class="k">do</span> <span class="p">{</span>
		<span class="kt">int</span> <span class="n">event_type</span> <span class="o">=</span> <span class="n">EvntMulti</span> <span class="p">(</span><span class="o">&amp;</span><span class="n">ev</span><span class="p">);</span>

		<span class="k">if</span> <span class="p">(</span><span class="n">event_type</span> <span class="o">&amp;</span> <span class="n">MU_TIMER</span><span class="p">)</span> <span class="p">{</span>		// <b class="conum">(3)</b>
			<span class="cm">/* when the timer event occurs, we need to update our display */</span>
			<span class="n">GRECT</span> <span class="n">rec</span><span class="p">;</span>

			<span class="n">wind_get</span> <span class="p">(</span><span class="n">wd</span><span class="o">-&gt;</span><span class="n">handle</span><span class="p">,</span> <span class="n">WF_WORKXYWH</span><span class="p">,</span> <span class="o">&amp;</span><span class="n">rec</span><span class="p">.</span><span class="n">g_x</span><span class="p">,</span> <span class="o">&amp;</span><span class="n">rec</span><span class="p">.</span><span class="n">g_y</span><span class="p">,</span> <span class="o">&amp;</span><span class="n">rec</span><span class="p">.</span><span class="n">g_w</span><span class="p">,</span> <span class="o">&amp;</span><span class="n">rec</span><span class="p">.</span><span class="n">g_h</span><span class="p">);</span>
			<span class="n">do_redraw</span> <span class="p">(</span><span class="n">wd</span><span class="p">,</span> <span class="o">&amp;</span><span class="n">rec</span><span class="p">);</span>
		<span class="p">}</span>

		<span class="k">if</span> <span class="p">(</span><span class="n">event_type</span> <span class="o">&amp;</span> <span class="n">MU_MESAG</span><span class="p">)</span> <span class="p">{</span>

			<span class="c1">// USUAL TOP/MOVE/REDRAW events</span>

		<span class="p">}</span>
	<span class="p">}</span> <span class="k">while</span> <span class="p">(</span><span class="n">ev</span><span class="p">.</span><span class="n">ev_mmgpbuf</span><span class="p">[</span><span class="mi">0</span><span class="p">]</span> <span class="o">!=</span> <span class="n">WM_CLOSED</span><span class="p">);</span>
<span class="p">}</span></code></pre>
</div>
</div>
<div class="colist arabic">
<ol>
<li>
<p>Set up the listener for AES menu/window events and timer events.</p>
</li>
<li>
<p>Set up the timer count to fire every second.</p>
</li>
<li>
<p>After receiving an event, check if it is a timer event, and redraw the display
if so.</p>
</li>
</ol>
</div>
</div>
<div class="sect2">
<h3 id="_sample_program_version_7">12.4. Sample Program: Version 7</h3>
<div class="paragraph">
<p>In version 7 of the program, we include a menu.  The menu provides an
&#39;about&#39; dialog, a way to quit the program, and a menu to select which poem
to show.</p>
</div>
<div class="paragraph">
<p>The menu was created in Resource Master, and saved into the RSC file.  The
C header was exported, creating the .rsh file.  The .rsh file is needed for
compiling, and the RSC file for running the program.</p>
</div>
<div class="paragraph">
<p>The &#39;Quit&#39; option may be triggered via the menu or by keyboard, so this version
uses multiple events in the event loop; two event loops are provided, one
for the traditional approach, and one using AHCC’s custom one.  The &#39;About&#39;
option displays a simple dialog box: this uses the built in <code>form_alert</code> call
to construct a dialog. I explain more about this and dialogs in general in
the next section.</p>
</div>
<div class="paragraph">
<p>Finally, this version allows us to open and close poems as we wish.  So the
window creation code has been changed to allow for a more dynamic set of
windows, and the window close code has been changed, to only close the selected
window, not exit the program.  The program is only exited with the &#39;QUIT&#39;
option.</p>
</div>
</div>
</div>
</div></body>
</div>
