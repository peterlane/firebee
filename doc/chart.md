# Chart

Chart provides a library of simple chart functions. The code can be embedded
into an existing project by including the `chart.h` and `chart.c` files. 
`chart.h` contains a description of available functions. 

<img src="images/chart.jpg" alt="Screenshot of Chart example running on a Firebee" width="40%" />

Above is a screenshot of the Chart example running under MINT+XaAES on a
Firebee. 

<img src="images/chart-st.png" alt="Screenshot of Chart example running in high resolution on an Atari ST" width="40%" />

Above is a screenshot of the Chart example running in high resolution on an
Atari ST.

Three types of chart are supported:

1. Bar Charts: A set of positive values is converted into a series of vertical bars. The title, x/y labels, colours and fill patterns of bars may be altered.
2. Line Charts: A set of lines added to a line chart. The title, x/y labels, and colour, line style and point style may all be altered.
3. Pie Charts: A set of positive values is converted into slices of a pie chart. The title, slice labels, colours and fill patterns may be altered.


