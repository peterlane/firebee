# Firebee

If you have downloaded the source code for this project, you will be looking
at this file along with the following:

* `bibfind` - source code for the BibFind program.
* `chart` - source code for the Chart library.
* `cmanship` - CManShip examples rewritten to work with AHCC.
* `doc` - the gemguide in html form, and overview pages for the following 
  projects. These documents are for viewing within the fossil repository.
* `gemguide` - the GEM guide source. The rakefile can be used to build the 
  html and pdf versions of the guide.
* `sokoban` - source code for Sokoban program.

